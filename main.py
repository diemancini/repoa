def sum(num_a: int, num_b: int) -> int:

    total: int = num_a + num_b

    return total

if __name__:

    num_a: int = input("Put fist number for sum:")
    num_b: int = input("Put second number for sum:")

    total: int = sum(int(num_a), int(num_b))

    print("The sum between {} and {} is {}".format(num_a, num_b, total))
